import Vue from 'vue';
import Router from 'vue-router';
import AgendaView from '@/views/Agenda.vue';
import ClientesView from '@/views/Clientes.vue';
import ProfissionaisView from '@/views/Profissionais.vue';
import EstoqueView from '@/views/Estoque.vue';
import RelatoriosView from '@/views/Relatorios.vue';
import ConfiguracoesView from '@/views/Configuracoes.vue';
import UsuariosView from '@/views/Usuarios.vue';
import LogsView from '@/views/Logs.vue';
import SemPermissaoView from '@/views/SemPermissao.vue';
import Page404View from '@/views/Page404.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/agenda'
        },
        {
            name: 'agenda',
            path: '/agenda',
            component: AgendaView
        },
        {
            name: 'clientes',
            path: '/clientes',
            component: ClientesView
        },
        {
            name: 'profissionais',
            path: '/profissionais',
            component: ProfissionaisView
        },
        {
            name: 'estoque',
            path: '/estoque',
            component: EstoqueView
        },
        {
            name: 'relatorios',
            path: '/relatorios',
            component: RelatoriosView
        },
        {
            path: '/configuracoes',
            component: ConfiguracoesView
        },
        {
            name: 'usuarios',
            path: '/usuarios',
            component: UsuariosView
        },
        {
            name: 'logs',
            path: '/logs',
            component: LogsView
        },
        {
            path: '/*!~semPermissao',
            component: SemPermissaoView
        },
        {
            path: '*',
            component: Page404View
        }
    ],
    mode: 'history'
});
