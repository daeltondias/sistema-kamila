/* eslint-disable */
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import VueCookie from 'vue-cookie';
import firebase from 'firebase/app';
import moment from 'moment';
import $ from 'jquery';
import 'firebase/firestore';
import 'bootstrap';
import 'bootstrap-notify';
import 'jquery-mask-plugin';
import 'jquery-ui/ui/widgets/datepicker';
import 'jquery-ui/ui/i18n/datepicker-pt-BR';
import 'moment/locale/pt-br';
//import 'popper.js';

if (typeof JSON.clone !== "function") {
  JSON.clone = function(obj) {
    return JSON.parse(JSON.stringify(obj));
  };
}

firebase.initializeApp({
    apiKey: process.env.VUE_APP_APIKEY,
    authDomain: process.env.VUE_APP_AUTHDOMAIN,
    databaseURL: process.env.VUE_APP_DATABASEURL,
    projectId: process.env.VUE_APP_PROJECTID,
    storageBucket: process.env.VUE_APP_STORAGEBUCKET,
    messagingSenderId: process.env.VUE_APP_MESSAGINGSENDERID,
    appId: process.env.VUE_APP_APPID
});

Vue.use(VueCookie);
Vue.config.productionTip = false;

Vue.directive('modal', function(el) {
  $(el).on('shown.bs.modal', function(){
    $(el).find('[autofocus]').focus();
  });
});

Vue.filter('formatDate', function(value, format){
  if (value) {
    return moment(value.toDate()).format(format);
  }
});

Vue.directive('tooltip-title', function(el, binding) {
  $(el).tooltip({
    placement: el.getAttribute('tooltip-placement'),
    title: binding.value,
    trigger: 'hover',
    html: true
  }).on('click', function() {
    $(el).tooltip('hide');
  });
});

Vue.directive('popover-title', function(el, binding, vnode) {
  var contentElement = el.getAttribute('popover-content');
  if (contentElement == 'confirm-delete') {
    contentElement = `
      <div class="d-flex justify-content-end">
        <a href="#" class="yes btn btn-danger btn-sm mr-2">Sim, deletar</a>
        <a href="#" class="cancel btn btn-secondary btn-sm">Cancelar</a>
      </div>
    `;
  }
  $(el).popover({
    placement: el.getAttribute('popover-placement'),
    content: contentElement,
    title: binding.value,
    trigger: 'focus',
    html: true
  }).on('shown.bs.popover', function(){
    $('.popover .yes').off('click').on('click', function(e){
      e.preventDefault();
      vnode.context.deletar(el.id);
    });
    $('.popover .cancel').off('click').on('click', function(e){
      e.preventDefault();
    });
  });
});

Vue.directive('mask', function(el, binding) {
  if (binding.value == 'celphone') {
    var SPMaskBehavior = function(val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
      onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      }
    };
    $(el).mask(SPMaskBehavior, spOptions);
  } else if (binding.value == 'money') {
    $(el).mask('#.##0,00', { reverse: true });
  } else {
    $(el).mask(binding.value);
  }
});

Vue.directive('datepicker', function(el, binding, vnode){
  $(el).datepicker({
    dateFormat: 'dd/mm/yy',
    onSelect: function(date) {
      vnode.context.dataModal.dataNascimento = date;
      vnode.context.validaDados({});
    }
  });
});

new Vue({
  data: {
    teste: [],
    usuario: {
      permissoes: {}
    },
    estadosBrasileiros: {
      'AC': 'Acre',
      'AL': 'Alagoas',
      'AP': 'Amapá',
      'AM': 'Amazonas',
      'BA': 'Bahia',
      'CE': 'Ceará',
      'DF': 'Distrito Federal',
      'ES': 'Espírito Santo',
      'GO': 'Goiás',
      'MA': 'Maranhão',
      'MT': 'Mato Grosso',
      'MS': 'Mato Grosso do Sul',
      'MG': 'Minas Gerais',
      'PA': 'Pará',
      'PB': 'Paraíba',
      'PR': 'Paraná',
      'PE': 'Pernambuco',
      'PI': 'Piauí',
      'RJ': 'Rio de Janeiro',
      'RN': 'Rio Grande do Norte',
      'RS': 'Rio Grande do Sul',
      'RO': 'Rondônia',
      'RR': 'Roraima',
      'SC': 'Santa Catarina',
      'SP': 'São Paulo',
      'SE': 'Sergipe',
      'TO': 'Tocantins'
    }
  },
  methods: {
    authentication() {
      const self = this;
      let session = this.$cookie.get('session');
      if (!session) {
        this.$router.push('/login');
      } else if (this.$route.name && !this.usuario.permissoes[this.$route.name] && !this.$route.path.includes('!~semPermissao') && this.usuario.tipoConta != 'Master' && !this.usuario.admin) {
        this.$router.push(this.$route.path + '!~semPermissao');
      } else if (this.$route.path == '/login') {
        this.$router.push('/configuracoes', function(){
          setTimeout(function(){
            var path = $('#sidebar a')[0].pathname;
            self.$router.push(path);
          }, 1);
        });
        //for (var key in this.usuario.permissoes) {
        //  if (this.usuario.permissoes[key]) {
        //    this.$router.push({name: key});
        //    break;
        //  }
        //}
      }
    },
    preloaderStart(callback) {
      $('#preloaderFullscreen').fadeIn();
      setTimeout(function(){
        callback();
      }, 2000);
    },
    preloaderEnd() {
      $('#preloaderFullscreen').fadeOut();
    },
    notify(message, type) {
      var icon = type;
      switch (type) {
        case 'sucesso':
          type = 'success';
          icon = 'check_circle';
          break;
        case 'aviso':
          type = icon = 'warning';
          break;
        case 'erro':
          type = 'danger';
          icon = 'cancel';
          break;
      }
      $.notify({
        icon: 'material-icons ' + icon,
        message: message,
      }, {type: type});
    },
    filter(array, search) {
      return array.filter((item) => {
          for (var key in item) {
              if (String(item[key]).toLowerCase().match(search.toLowerCase())) return item;
          }
      });
    },
    removeKeys(array) {
      return {
        from(obj) {
          array.forEach(function(key){
            delete obj[key];
          });
        }
      };
    },
    isCPFValid(strCPF) {
      strCPF = strCPF.replace(/[^0-9]/g,'');
      var Soma = 0, Resto;
      if (strCPF == "00000000000") return false;
      for (var i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
      Resto = (Soma * 10) % 11;
      if ((Resto == 10) || (Resto == 11))  Resto = 0;
      if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
      Soma = 0;
      for (var i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
      Resto = (Soma * 10) % 11;
      if ((Resto == 10) || (Resto == 11))  Resto = 0;
      if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
      return true;
    },
    deleteAllDocsFromFirestore() {
      const rootSelf = this;
      return {
        fromCollection(i) {
          firebase.firestore().collection(i).get().then(function(docs){
              docs.forEach(function(doc){
                  doc.ref.delete();
              });
          });
        },
        fromAllCollection() {
          const self = this;
          ['logs', 'agenda', 'clientes', 'profissionais', 'produtos'].forEach(function(collectionName){
            self.fromCollection(collectionName);
          });
          rootSelf.preloaderStart(function(){
            setTimeout(function(){
              rootSelf.preloaderEnd();
            }, 2000);
          });
        }
      };
    },
    isEmptyOrUndefined(data) {
      return (data == null || data == undefined || data == '' ? true : false);
    }
  },
  router,
  render: h => h(App),
}).$mount('#app');
